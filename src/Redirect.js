import React, { useEffect, useState } from "react";

const Redirect = () => {
  const [url, setUrl] = useState("");
  useEffect(() => {
    setUrl(window.location.href);
  }, []);
  const [id, setId] = useState();

  useEffect(() => {
    const array = url.split("/");
    setId(array[array.length - 1]);
  }, [url]);

  useEffect(() => {
    const timer = setTimeout(() => {
      window.location.href = `http://192.168.0.20:3000`;
      setTimeout(() => {
        window.location.href = `http://192.168.0.20:3000`;
      }, 1000);
    }, 2000);
    return () => clearTimeout(timer);
  }, [id]);
  return (
    <div>
      <div>Redirecting</div>
    </div>
  );
};

export default Redirect;
