import "./App.css";
import Redirect from "./Redirect";
import { BrowserRouter, Route } from "react-router-dom";

function App() {
  return (
    <div>
      <BrowserRouter>
        {/* <Route path="/" exact component={Main} /> */}
        <Route path="/" component={Redirect} />
      </BrowserRouter>
    </div>
  );
}

export default App;
